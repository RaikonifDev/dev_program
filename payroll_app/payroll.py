# missing __init__ (it means contructor call uses default __init__ (dummy method, 0 arguments expected))
# Line 8 prints object (of class Employee) reference - Better to overwrite __str__ inherited from object()

class PayrollSystem:

    # interface (access point) NOT OOP Interface
    def calculate_payroll(self, employees):
        result = [ (employee, employee.calculate_payroll()) for employee in employees]
        [ print(f"Payroll for: {e}\nCheck amount: {p}") for e, p in result ]
        return result

        # for employee in employees:
        #     print(f'Payroll for: {employee}')
        #     print(f'Check amount: {employee.calculate_payroll()}')
